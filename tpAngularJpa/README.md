TP Angular + JPA
===================

Deuxième TP à rendre dans le cadre de la formation **SIR**.
Ce TP contient une implémentation du framework **Angular** avec l'ORM **JPA**.

----------


#### Lancer le projet

Nous utilisons l'outil de génération Yeomann, donc vérifier que toutes les dépendences sont bien installées
> npm install -g grunt-cli bower yo generator-karma generator-angular
> bower install

Pour faire fonctionner le projet, nous avons besoin d'une part : Projet Maven pour services REST (voir premier TP) (port 8080)
> tomcat7:run sur le projet tpServletJpa

Et d'autre part, grunt pour lancer un serveur local (port 9000).
> grunt serve

#### Framework Angular

Une fois le serveur lancé, vous pouvez accéder à l'homepage de l'application (url : http://localhost:9000/#/).

Elle permet de tester les différentes fonctionnalités d'Angular, parmi elles :


###Comprendre la notion de models

    //model
    {
      "name" : String,
      "address" : String,
      "private" : bool
    };


###Comprendre la notion de directive
    .directive('monLieu', function() {
      return {
        template: 'Name: {{lieu.name}} - Address: {{lieu.address}} - Private {{lieu.private}}'
      };
    }

###Comprendre la notion de service niveau 1 $log

html

    Name : <input type="text" name="name" ng-model="lieu.name" ng-change="change(lieu.name)"/>

js

    $scope.$log = $log;
    $scope.message = 'Change';

    $scope.change = function(field) {
      $log.log(field)
    };

###Comprendre la notion de service niveau 2 $http

html


    <textarea cols="50" rows="5"ng-model="response"></textarea>
    <br/>

    <input type="text" name="servicehttp" ng-model="request"/>
    <button ng-click="httpget()">GET</button>

js

    $scope.request = "/test";

    $scope.httpget = function(){

      $http.get($scope.request).
      success(function(data, status, headers, config) {
        $scope.response = "Status code : " + status + "\nData : " + data;
      }).
      error(function(data, status, headers, config) {
        $scope.response = "Status code : " + status + "\nData : " + data;
      });
    };

    
### Accéder aux différentes maisons du serveur Jersey


Nous avons également une page qui récupère les maisons via le service REST Jersey à cette adresse : http://localhost:9000/#/homes

Nous l'avons générée à l'aide du générateur Yeoman.

Nous avons dans le controlleur :
> $http.get('http://localhost:8080/rest/homes')
>    .success(function(data, status, headers, config) {
>      $scope.response = "Status code : " + status + "\nData : " + data;
>      console.log(data);
>      $scope.homes = data;
>    })

Ce qui nous permet de remplir le tableau dans la vue homes.html