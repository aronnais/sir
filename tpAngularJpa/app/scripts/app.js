'use strict';

/**
 * @ngdoc overview
 * @name myappApp
 * @description
 * # myappApp
 *
 * Main module of the application.
 */
angular
  .module('myappApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
    	  templateUrl: 'views/superapp.html',
          controller: 'SuperappCtrl'
      })
      .when('/homes', {
        templateUrl: 'views/homes.html',
        controller: 'HomesCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
