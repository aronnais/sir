'use strict';

/**
 * @ngdoc function
 * @name myappApp.controller:HomesCtrl
 * @description
 * # HomesCtrl
 * Controller of the myappApp
 */
angular.module('myappApp')
  .controller('HomesCtrl', function ($scope, $http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $http.get('http://localhost:8080/rest/homes')
    .success(function(data, status, headers, config) {
      $scope.response = "Status code : " + status + "\nData : " + data;
      console.log(data);
      $scope.homes = data;
    })
  });
