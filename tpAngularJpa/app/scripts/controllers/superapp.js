'use strict';

/**
* @ngdoc function
* @name myappApp.controller:SuperappCtrl
* @description
* # SuperappCtrl
* Controller of the myappApp
*/
angular.module('myappApp')
.controller('SuperappCtrl', ['$scope', '$log', '$http', function ($scope, $log, $http) {

  //model
  $scope.lieu = {
    "name" : "Istic",
    "address" : "Campus de Beaulieu, Rennes",
    "private" : false
  };

  $scope.$log = $log;
  $scope.message = 'Change';

  $scope.change = function(field) {
    $log.log(field)
  };

  $scope.request = "/test";

  $scope.httpget = function(){

    $http.get($scope.request).
    success(function(data, status, headers, config) {
      $scope.response = "Status code : " + status + "\nData : " + data;
    }).
    error(function(data, status, headers, config) {
      $scope.response = "Status code : " + status + "\nData : " + data;
    });
  };


}])
.directive('monLieu', function() {
  return {
    template: 'Name: {{lieu.name}} - Address: {{lieu.address}} - Private {{lieu.private}}'
  };
});
