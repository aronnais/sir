'use strict';

describe('Controller: SuperappCtrl', function () {

  // load the controller's module
  beforeEach(module('myappApp'));

  var SuperappCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SuperappCtrl = $controller('SuperappCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
