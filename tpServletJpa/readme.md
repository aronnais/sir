TP Servlet + JPA
===================

Premier TP à rendre dans le cadre de la formation **SIR**.
Ce TP contient une implémentation du framework **Jersey** avec l'ORM **JPA**.

----------


#### Importer le projet

Il faut tout d'abord importer le projet Maven à l'aide de votre IDE préféré. Voici la procédure dans Eclipse :

> **Importer projet Maven:**

> - File --> Import
> - Existing Maven Project
> - Select git repository
> - Check the pom.xml checkbox

Vous pouvez maintenant lancer Maven à l'aide d'Apache Tomcat en définissant le goal dans l'outil run tel que :

> tomcat7:run

#### Premières Servlets

Une fois le serveur lancé, vous pouvez accéder à l'homepage de l'application (url : http://localhost:8080), elle liste les différents liens accessibles.

> - Hello world : http://localhost:8080/welcome
> - Formulaire User : http://localhost:8080/myform.html
> - Formulaire POST Device : http://localhost:8080/addDevice.html
> - Lister les devices : http://localhost:8080/device

#### Service sous Jersey

Une interface à été créée à l'aide du framework Jersey. Elle permet par exemple de :

> - Lister les maisons :
> GET http://localhost:8080/rest/homes
> - Chercher une maison :
> GET http://localhost:8080/rest/home/{id}
> - Ajouter une maison :
> POST http://localhost:8080/rest/home
> - Supprimer une maison : 
> DELETE http://localhost:8080/rest/home/{id}
> - Modifier une maison :
> PUT http://localhost:8080/rest/home/{id}

Le code de l'interface SampleWebService se trouve dans src/main/java/fr.istic.sir.rest.
