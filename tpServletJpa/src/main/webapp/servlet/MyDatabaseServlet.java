package servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.PluralAttribute;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.istic.tpjpa.jpa.Device;
import fr.istic.tpjpa.jpa.ElecDevice;
import fr.istic.tpjpa.jpa.JpaTest;

@WebServlet(name="devices", urlPatterns={"/device"})
public class MyDatabaseServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html");
		
		ServletOutputStream out = resp.getOutputStream();
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("example");
		EntityManager manager = factory.createEntityManager();
		
		TypedQuery<Device> query = manager.createQuery("SELECT d FROM Device d", Device.class);

		// create query and execute...
		List<Device> res = query.getResultList();
		
		out.println("<table>");
		out.println("<thead>");
		out.println("<tr>");
		out.println("<th>Nom</th>");
		out.println("<th>Puissance</th>");
		out.println("</tr>");
		out.println("</thead>");
		out.println("<tbody>");
		for(Device d : res) {
			out.println("<tr>");
			out.println("<td>");
			out.println(d.getNom());
			out.println("</td>");
			out.println("<td>");
			out.println(d.getPuissance());
			out.println("</td>");
			out.println("</tr>");
		}
		out.println("</tbody>");
		out.println("</table>");
		
		resp.getOutputStream().flush();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}
}