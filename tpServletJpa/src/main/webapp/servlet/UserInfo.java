package servlet;
 

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="userInfo", urlPatterns={"/UserInfo"})
public class UserInfo extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html");
		
		ServletOutputStream out = resp.getOutputStream();
		
		out.println("<html>");
		out.println("<body>");
		
		out.println("<h1>Bonjour " + req.getParameter("name") + " " + req.getParameter("firstname") + ", savez-vous que vous avez " + req.getParameter("age") + " ans ?</h1>");
		
		out.println("</body>");
		out.println("</html>");
	}
}
