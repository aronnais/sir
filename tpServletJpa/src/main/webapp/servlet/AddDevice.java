package servlet;
 

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.istic.tpjpa.jpa.ElecDevice;

@WebServlet(name="addDevice", urlPatterns={"/device/add"})
public class AddDevice extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html");
		
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("example");
		EntityManager manager = factory.createEntityManager();
		
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		
		ElecDevice ch = new ElecDevice(req.getParameter("nom"), Float.parseFloat(req.getParameter("puissance")), req.getParameter("classe"));
		
		manager.persist(ch);
		tx.commit();
		
		
		ServletOutputStream out = resp.getOutputStream();
		
		out.println("<html>");
		out.println("<body>");
		
		out.println("<h1>L'appareil electrique " + req.getParameter("nom") + " d'une pusisance de " + req.getParameter("puissance") + " (" + req.getParameter("classe") + ") a bien �t� ajout�</h1>");
		
		out.println("</body>");
		out.println("</html>");
	}
}
