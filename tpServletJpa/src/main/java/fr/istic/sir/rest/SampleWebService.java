package fr.istic.sir.rest;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.annotate.JsonIgnore;

import fr.istic.tpjpa.jpa.Maison;

@Path("/")
public class SampleWebService {

	private List<Maison> homes;
	private EntityManager manager;

	public SampleWebService(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("example");
		manager = factory.createEntityManager();	
		TypedQuery<Maison> query = manager.createQuery("SELECT h FROM Maison h", Maison.class);
		// create query and execute...
		homes = query.getResultList();
	}

	@GET
	@Path("/homes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@JsonIgnore
	public Collection<Maison> getHomes(@Context HttpHeaders header) {
		return homes;
	}

	@GET
	@Path("/home/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Maison getHomeById(@PathParam("id") String id) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Maison> query = criteriaBuilder.createQuery(Maison.class);
		Root<Maison> from = query.from(Maison.class);
		query.where(criteriaBuilder.equal(from.get("num"), id));

		System.out.println("in here");
		return manager.createQuery(query).getSingleResult();
	}

	@POST
	@Path("/home/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Maison postHome() {
		return null;
	}
	
	@DELETE 
	@Path("/home/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public void removeHomeById(@PathParam("id") int id) {

		EntityTransaction transaction = manager.getTransaction();
		transaction.begin();

		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaDelete<Maison> delete = criteriaBuilder.createCriteriaDelete(Maison.class);
		Root<Maison> maison = delete.from(Maison.class);
		delete.where(criteriaBuilder.equal(maison.get("num"), id));
		manager.createQuery(delete).executeUpdate();

		transaction.commit();
	}

}
