package fr.istic.tpjpa.jpa;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@DiscriminatorColumn(name="E")
public class ElecDevice extends Device {
	
	private String classeEfficaciteEnergetique;
	
	public ElecDevice() {
		super();
	}
	
	/**
	 * @param classeEfficaciteEnergetique
	 */
	public ElecDevice(String nom, float puissance, String classeEfficaciteEnergetique) {
		super(nom, puissance);
		this.classeEfficaciteEnergetique = classeEfficaciteEnergetique;
	}
	
	/**
	 * @return the classeEfficaciteEnergetique
	 */
	public String getClasseEfficaciteEnergetique() {
		return classeEfficaciteEnergetique;
	}

	/**
	 * @param classeEfficaciteEnergetique the classeEfficaciteEnergetique to set
	 */
	public void setClasseEfficaciteEnergetique(String classeEfficaciteEnergetique) {
		this.classeEfficaciteEnergetique = classeEfficaciteEnergetique;
	}

}
