package fr.istic.tpjpa.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Device {
	
	private int num;
	private String nom;
	private float puissance;
	private Maison maison;
	
	
	/**
	 * 
	 */
	public Device()
	{
		
	}


	/**
	 * @param nom
	 * @param puissance
	 */
	public Device(String nom, float puissance) {
		super();
		this.nom = nom;
		this.puissance = puissance;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @return the puissance
	 */
	public float getPuissance() {
		return puissance;
	}


	/**
	 * @return the num
	 */
	@Id
	@GeneratedValue
	public int getNum() {
		return num;
	}


	/**
	 * @param num the num to set
	 */
	public void setNum(int num) {
		this.num = num;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @param puissance the puissance to set
	 */
	public void setPuissance(float puissance) {
		this.puissance = puissance;
	}

	
	@ManyToOne
	@JsonBackReference
	public Maison getMaison() {
		return maison;
	}
	
	public void setMaison(Maison maison) {
		this.maison = maison;
	}
	
}
