package fr.istic.tpjpa.jpa;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class JpaTest {

	private EntityManager manager;

	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("example");
		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);

		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		
		Personne p1 = new Personne();
		manager.persist(p1);
		
		List<Personne> amis = new ArrayList<Personne>();
		for (int i = 0; i < 10; i++) {
			amis.add(genererPersonne());
		}
		
		List<Maison> domiciles = new ArrayList<Maison>();
		for (int i = 0; i < 3; i++) {
			domiciles.add(genererDomicile());
			domiciles.get(i).setPersonne(p1);
		}
		
		for(Maison m : domiciles) {
			List<Device> devices = new ArrayList<Device>();
			for(int i = 0 ; i < 10; i++) {
				devices.add(genererDevice());
				devices.get(i).setMaison(m);
			}
			m.setAppareils(devices);
		}
		
		p1 = new Personne("Petit", "Emmanuel", "epetit11@gmail.com", "emmanuel.petit.52", "M", new Date(1993, 3, 2), amis, domiciles);
		manager.persist(p1);
		tx.commit();

		// TODO run request
		
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		// assuming a is an Integer  
		// select b.a from B b where a in (1, 2, 3, 4)
		// if returning multiple fields, look into using a Tuple 
//		    or specifying the return type as an Object or Object[]
		
		CriteriaQuery<String> query = criteriaBuilder.createQuery(String.class);
		Root<ElecDevice> from = query.from(ElecDevice.class);
		query.multiselect(from.get("nom"))
		     .where(from.get("classeEfficaciteEnergetique").in("A", "B", "C"));

		// create query and execute...
		List<String> res = manager.createQuery(query).getResultList();
		
		for(String s : res) {
			System.out.println("\t\t" + s);
		}
		
		System.out.println(".. done");
	}
	
	private static Device genererDevice() {
		Random r = new Random();
		Device device;
		String nom = new NameGenerator().getName();
		int puissance = r.nextInt(2000);
		if(r.nextInt()%2==0) {
			device = new Chauffage(nom, puissance, r.nextInt(1000));
		} else {
			switch (r.nextInt(10)%5) {
			case 1:
				device = new ElecDevice(nom, puissance, "A");
				break;
			case 2:
				device = new ElecDevice(nom, puissance, "B");
				break;
			case 3:
				device = new ElecDevice(nom, puissance, "C");
				break;
			case 4:
				device = new ElecDevice(nom, puissance, "D");
				break;
			case 5:
				device = new ElecDevice(nom, puissance, "E");
				break;
			default:
				device = new ElecDevice(nom, puissance, "A");
				break;
			}
			
		}
		return device;
	}

	private static Maison genererDomicile() {
		Random r = new Random();
		
		Maison m = new Maison();
		m.setAdresse(r.nextInt(100) + " rue " + new NameGenerator().getName());
		m.setSuperficie(r.nextInt(300));
		m.setIp(r.nextInt(255) + "." + r.nextInt(255) + "." + r.nextInt(255) + "." + r.nextInt(255));
		return m;
	}

	private static Personne genererPersonne(){
		Personne p = new Personne();
		
		p.setNom(new NameGenerator().getName());
		p.setPrenom(new NameGenerator().getName());
		p.setMail(p.getNom() + "@" + p.getPrenom() + ".com");
		p.setProfil(p.getNom() + "." + p.getPrenom());
		p.setDateDeNaissance(new Date(System.currentTimeMillis()));
		p.setGenre("M");	
		
		return p;
	}
	

}
