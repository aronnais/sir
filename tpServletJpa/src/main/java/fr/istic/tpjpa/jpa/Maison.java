package fr.istic.tpjpa.jpa;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
public class Maison {

	private long num;
	private String adresse;
	private int	superficie;
	private String ip;
	private List<Device> appareils;
	private float consommationMaison;
	private Personne personne;
	
	/**
	 *  Constructeur
	 */
	public Maison(){
		
	}

	
	/**
	 * 
	 * @param adresse de la maison
	 * @param superficie de la maison
	 * @param ip de la box de la maison
	 */
	public Maison(String adresse, int superficie, String ip, List<Device> appareils, Personne personne) {
		super();
		this.setAdresse(adresse);
		this.setSuperficie(superficie);
		this.ip = ip;
		this.appareils = appareils;
		this.personne = personne;
	}
	
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	

	/**
	 * @return the num
	 */
	@Id
	@GeneratedValue
	public long getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(long num) {
		this.num = num;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the superficie
	 */
	public int getSuperficie() {
		return superficie;
	}

	/**
	 * @param superficie the superficie to set
	 */
	public void setSuperficie(int superficie) {
		this.superficie = superficie;
	}


	/**
	 * @return the appareils
	 */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="maison", fetch=FetchType.EAGER)
	@JsonBackReference
	public List<Device> getAppareils() {
		return appareils;
	}


	/**
	 * @param appareils the appareils to set
	 */
	public void setAppareils(List<Device> appareils) {
		this.appareils = appareils;
	}


	/**
	 * @return the consommationMaison
	 */
	@Transient
	public float getConsommationMaison() {
		return consommationMaison;
	}


	/**
	 * @param consommationMaison the consommationMaison to set
	 */
	public void setConsommationMaison(float consommationMaison) {
		this.consommationMaison = consommationMaison;
	}
	
	@ManyToOne
	@JsonBackReference
	public Personne getPersonne() {
		return personne;
	}
	
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	
}
