/**
 * 
 */
package fr.istic.tpjpa.jpa;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@DiscriminatorColumn(name="C")
public class Chauffage extends Device {
	
	private float chaleurGeneree;
	
	/**
	 * @return the chaleurGeneree
	 */
	public float getChaleurGeneree() {
		return chaleurGeneree;
	}

	/**
	 * @param chaleurGeneree the chaleurGeneree to set
	 */
	public void setChaleurGeneree(float chaleurGeneree) {
		this.chaleurGeneree = chaleurGeneree;
	}

	public Chauffage() {
		super();
	}

	/**
	 * @param chaleurGeneree
	 */
	public Chauffage(String nom, float puissance, float chaleurGeneree) {
		super(nom, puissance);
		this.chaleurGeneree = chaleurGeneree;
	}
	
	
	
}
