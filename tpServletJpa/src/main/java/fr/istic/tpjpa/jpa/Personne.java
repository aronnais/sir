package fr.istic.tpjpa.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
public class Personne {
	
	private long num;
	private String nom, prenom, mail, profil, genre;
	private Date dateDeNaissance;
	private List<Personne> amis;
	private List<Maison> domiciles;
	
	public Personne()
	{
		
	}

	public Personne(String nom, String prenom, String mail, String profil,
			String genre, Date dateDeNaissance, List<Personne> amis,
			List<Maison> domiciles) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.profil = profil;
		this.genre = genre;
		this.dateDeNaissance = dateDeNaissance;
		this.amis = amis;
		this.domiciles = domiciles;
	}

	/**
	 * @return the amis
	 */
	@OneToMany(cascade=CascadeType.ALL)
	@Column(name="AMIS")
	@JsonBackReference
	public List<Personne> getAmis() {
		return amis;
	}

	/**
	 * @param amis the amis to set
	 */
	public void setAmis(List<Personne> amis) {
		this.amis = amis;
	}

	/**
	 * @return the domiciles
	 */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="personne")
	@JsonBackReference
	public List<Maison> getDomiciles() {
		return domiciles;
	}

	/**
	 * @param domiciles the domiciles to set
	 */
	public void setDomiciles(List<Maison> domiciles) {
		this.domiciles = domiciles;
	}

	@Id
	@GeneratedValue
	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	
	

}
