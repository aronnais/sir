package fr.istic.esnaultpetit.sir.tpgwt.client;

import org.eclipse.jetty.util.ajax.JSON;

import fr.istic.esnaultpetit.sir.tpgwt.shared.FieldVerifier;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.thirdparty.javascript.jscomp.CompilerOptions.Reach;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class tpgwt implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private final OpowerServiceAsync opowerService = GWT.create(OpowerService.class);

	private final Messages messages = GWT.create(Messages.class);
	
	private DialogBox dialogBox;
	
	private Label output;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		//Creation des boutons
		final Button helloWorldButton = new Button( messages.sendButton() );
		helloWorldButton.setHTML("Hello World");

		final Button listHomeButton = new Button( messages.sendButton() );
		listHomeButton.setHTML("Lister toutes les maisons");

		final Button searchHomeButton = new Button( messages.sendButton() );
		searchHomeButton.setHTML("Recherche");
		
		final Button addHomeButton = new Button( messages.sendButton() );
		addHomeButton.setHTML("Ajouter");

		final Button updateHomeButton = new Button( messages.sendButton() );
		updateHomeButton.setHTML("Modifier une maison");

		
		final Label errorLabel = new Label();	

		// Boutons
		RootPanel.get("hello_world").add(helloWorldButton);
		RootPanel.get("list_home").add(listHomeButton);
		
		//récupérer une maison
		final TextBox searchHomeId = new TextBox();
		RootPanel.get("search_home").add(searchHomeId);
		RootPanel.get("search_home").add(searchHomeButton);
		
		//ajouter une maison
		final Label adresseMaisonLabel = new Label();
		adresseMaisonLabel.setText("Adresse");		
		final TextBox adresseMaison = new TextBox();		
		RootPanel.get("add_home").add(adresseMaisonLabel);
		RootPanel.get("add_home").add(adresseMaison);

		final TextBox superficieMaison = new TextBox();
		final Label superficieMaisonLabel = new Label();
		superficieMaisonLabel.setText("Superficie");
		RootPanel.get("add_home").add(superficieMaisonLabel);
		RootPanel.get("add_home").add(superficieMaison);
		
		final TextBox ipMaison = new TextBox();
		final Label ipMaisonLabel = new Label();
		ipMaisonLabel.setText("Ip");
		RootPanel.get("add_home").add(ipMaisonLabel);
		RootPanel.get("add_home").add(ipMaison);
		
		final TextBox consomationMaison = new TextBox();
		final Label consomationMaisonLabel = new Label();
		consomationMaisonLabel.setText("Consomation");
		RootPanel.get("add_home").add(consomationMaisonLabel);
		RootPanel.get("add_home").add(consomationMaison);
				
		RootPanel.get("add_home").add(addHomeButton);
		
		/*
		RootPanel.get("update_home").add(updateHomeButton);
		*/
		
		/*
		final Button deleteHomeButton = new Button( messages.sendButton() );
		deleteHomeButton.setHTML("Supprimer");
		final Label deleteHomeIdLabel = new Label();
		deleteHomeIdLabel.setText("Id : ");		
		final TextBox deleteHomeId = new TextBox();
		RootPanel.get("delete_home").add(deleteHomeIdLabel);
		RootPanel.get("delete_home").add(deleteHomeId);
		RootPanel.get("delete_home").add(deleteHomeButton);
		*/
			
		RootPanel.get("errorLabelContainer").add(errorLabel);
		
		final HTML output = new HTML();		
		RootPanel.get("output").add(output);
		
		// Creation popup
		dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<br><b>Response :</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Fermeture popup
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});

		// Add HelloWorld handler
		helloWorldButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doGet(output, "http://127.0.0.1:8080/welcome");
			}

		});

		listHomeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doGet(output, "http://127.0.0.1:8080/rest/homes");
			}
		});

		searchHomeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doGet(output, "http://127.0.0.1:8080/rest/home/"+searchHomeId.getValue());
			}
		});

		/*
		addHomeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doPost(output, "http://127.0.0.1:8080/homes", "{}");
			}
		});
		
		updateHomeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doPut(output, "http://127.0.0.1:8080/homes", "");
			}
		});

		deleteHomeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doDelete(output, "http://127.0.0.1:8080/homes/", "");
			}
		});
		*/
	}
	
	public void doGet(final HTML output, final String url){
		
		output.setText("");
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);
		builder.setHeader("Content-type", "application/json");
		try {
			Request response = builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {

					output.setText(exception.getMessage());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					output.setHTML("<b>"+response.getStatusCode()+" GET " + url+"</b><br/>" + response.getText() + "");
				}
			});

		} catch (RequestException e) {
			// Code omitted for clarity
		}	
	}

	public void doPost(final HTML output, final String url, String requestData){

		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, url);
		builder.setHeader("Content-type", "application/x-www-form-urlencoded");
		try {
			Request response = builder.sendRequest(requestData, new RequestCallback() {
				public void onError(Request request, Throwable exception) {

					output.setText(exception.getMessage());
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					output.setHTML("<b>POST " + url+"</b><pre>" + response.getText() + "</pre>");
				}
			});

		} catch (RequestException e) {
			// Code omitted for clarity
		}	
	}

	public void doDelete(final HTML serverResponseLabel, final String url, String requestData){

		RequestBuilder builder = new RequestBuilder(RequestBuilder.DELETE, url);
		try {
			Request response = builder.sendRequest(requestData, new RequestCallback() {
				public void onError(Request request, Throwable exception) {

					dialogBox.setText("Erreur");
					serverResponseLabel.setText("");
					dialogBox.center();
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					dialogBox.setText("POST " + url);
					serverResponseLabel.setText(response.getText());
					dialogBox.center();
				}
			});

		} catch (RequestException e) {
			// Code omitted for clarity
		}	
	}
	

	public void doPut(final HTML serverResponseLabel, final String url, String requestData){

		RequestBuilder builder = new RequestBuilder(RequestBuilder.PUT, url);
		try {
			Request response = builder.sendRequest(requestData, new RequestCallback() {
				public void onError(Request request, Throwable exception) {

					dialogBox.setText("Erreur");
					serverResponseLabel.setText("");
					dialogBox.center();
				}

				@Override
				public void onResponseReceived(Request request, Response response) {
					dialogBox.setText("POST " + url);
					serverResponseLabel.setText(response.getText());
					dialogBox.center();
				}
			});

		} catch (RequestException e) {
			// Code omitted for clarity
		}	
	}
}
