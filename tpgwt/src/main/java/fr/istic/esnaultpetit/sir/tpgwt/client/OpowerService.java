package fr.istic.esnaultpetit.sir.tpgwt.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("opower")
public interface OpowerService extends RemoteService {
  String helloWorld() throws IllegalArgumentException;
}
