package fr.istic.esnaultpetit.sir.tpgwt.server;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.istic.esnaultpetit.sir.tpgwt.client.OpowerService;

@SuppressWarnings("serial")
public class OpowerServiceImpl extends RemoteServiceServlet implements OpowerService {

	String result;
	
	@Override
	public String helloWorld() throws IllegalArgumentException {

		String url = "http://127.0.0.1:8080/welcome";
		
		
		
	    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

	    try {
	      Request response = builder.sendRequest("", new RequestCallback() {
	        public void onError(Request request, Throwable exception) {
	          // Code omitted for clarity
	        }

			@Override
			public void onResponseReceived(Request request, Response response) {
				result = response.getText();
			}
	      });
	      
	    } catch (RequestException e) {
	      // Code omitted for clarity
	    }
	    
		return result;
	}


	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 *
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

}
