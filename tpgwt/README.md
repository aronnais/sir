TP GWT + JPA
===================

#### Importer le projet

> **Importer projet Maven:**

> - File --> Import
> - Existing Maven Project
> - Select git repository
> - Check the pom.xml checkbox

Lancer le serveur tomcat du projet tpServletJpa
> mvn tomcat7:run

Vous pouvez le Gwt
> mvn clean install
> mvn gwt:run


#### Page du projet http://127.0.0.1:8888/tpgwt.html

#### Envoie des requêtes GET, POST, PUT, DELETE au serveur JPA

> - Hello world : http://localhost:8080/welcome
> - Lister les maisons http://localhost:8080/homes
> - Afficher une maison http://localhost:8080/home/{id}